package com.example.adminn.appproj090216;

import android.content.ContentValues;
import android.content.Context;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.pdf.PdfDocument;
import android.os.Environment;
import android.print.PrintAttributes;
import android.print.pdf.PrintedPdfDocument;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.database.Cursor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private TableLayout ll;
    private ScrollView sv;
    private HorizontalScrollView hsv;
    private TextView tv1;
    private EditText et;
    private DataBase dbHelper;
    private String tableName;
    private String[] poleS = new String[11];
    private SQLiteDatabase db;
    private String DIR_SD = "MyFiles";
    private String FILENAME_SD = "fileSD.txt";
    private Cursor Tab1 = null;
    private Cursor cursorSL = null;
    private String string;
    private TableRow tbrow;
    private TableRow tRow;
    private ContentValues cv;
    private int baseId = 0;
    private List<EditText> list;
    private String tabHint;
    private BigDecimal bigDecimal;
    private Double aDouble;
    private BigDecimal decPdv= BigDecimal.ZERO;
    private BigDecimal decPay= BigDecimal.ZERO;
    private BigDecimal decPayPDV= BigDecimal.ZERO;
    private char[] chars = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'};
    private Integer id = 0;
    private String strId="";
    StringBuilder builder;
    StringBuilder builder2;
    private String month="03.16";
    private Counter counter;
    private  boolean flag=false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbHelper = new DataBase(this);
        db = dbHelper.getWritableDatabase();
        sv = new ScrollView(this);
        ll = new TableLayout(this);
        hsv = new HorizontalScrollView(this);
        cv = new ContentValues();
        tv1 = new TextView(this);
        list = new ArrayList<>();
        tableItemStart();
        tabHint = "X";
        cursorSL=db.query("H", null, "J='ZZ'", null, null, null, null);
        if (cursorSL.moveToFirst()){
            month=cursorSL.getString(1);
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_postach:
                tableName = "A";
                cursorGet();
                break;
            case R.id.action_casa:
                tableName = "B";
                cursorGet();
                break;
            case R.id.action_bank:
                tableName = "C";
                cursorGet();
                break;
            case R.id.action_PDVevryday:
                tableName = "D";
                cursorGet();
                break;
            case R.id.action_zp:
                tableName = "E";
                cursorGet();
                break;
            case R.id.action_zal:
                tableName = "F";
                cursorGet();
                break;
            case R.id.action_rez:
                tableName = "G";
                cursorGet();
                break;
            case R.id.action_table:
                tableName = "H";
                cursorGet();
                break;
            case R.id.action_SaveInBase:
                saveBase();
                break;
            case R.id.action_SaveInFile:
                saveFile();
                break;
            case R.id.action_Upgrade:
                updateBase();
                break;
            case R.id.action_Delete:
                delItem();
                break;
            case R.id.action_Hint:
                //run();//pdf document
                counter= new Counter();
                month=counter.countMonth(month);
                Toast.makeText(getBaseContext(), month, Toast.LENGTH_LONG).show();
                break;
            case R.id.action_goToUpd:
                gotoUpdate();
                break;
            case R.id.action_countCasa:
                //HTML doc
                //try {                    runHtml();                 } catch (FileNotFoundException e) {   e.printStackTrace();                }
                if (!flag){flag=true;}else{flag=false;}
                break;
            case R.id.action_SaveInFile2:
                saveInFile();
                break;
            case R.id.action_newCursor:
              zeroPdv();
                break;

            default:
                break;
        }


        return super.onOptionsItemSelected(item);
    }
    private void runHtml() throws FileNotFoundException {
        openFile("postachPattern.html");
        MakeHTML makeHTML;
        cursorSL = db.query("C",null,null,null,null,null,null);
        Tab1 = db.query("A",null,"F='o'",null,null,null,null);
        makeHTML = new MakeHTML("o",Tab1,cursorSL,builder,builder2);
        Tab1 = db.query("A",null,"F='v'",null,null,null,null);
        makeHTML = new MakeHTML("v",Tab1,cursorSL,builder,builder2);
        Tab1 = db.query("A",null,"F='k'",null,null,null,null);
        makeHTML = new MakeHTML("k",Tab1,cursorSL,builder,builder2);
        Tab1 = db.query("A",null,"F='f'",null,null,null,null);
        makeHTML = new MakeHTML("f",Tab1,cursorSL,builder,builder2);
}

private void zeroPdv(){
    decPdv=BigDecimal.ZERO;
    decPay=BigDecimal.ZERO;
    decPayPDV=BigDecimal.ZERO;
}


    private void cursorGet() {
        Log.d("Help", "Its cursorGet");
       /* if (tableName.equals("D")){
            Tab1 = db.query(tableName, null, null, null, null, null, "I ASC");//[ASC | DESC];
        }else {
            Tab1 = db.query(tableName, null, null, null, null, null, null);
        }*/
        cursorNew();

        Log.d("Help", "It s query");
        viewDataBase();
        setHint();
    }

    public void cursorNew(){
        switch (tableName){
            case "D":Tab1 = db.query(tableName, null, "J='"+month+"'", null, null, null, "I ASC");//[ASC | DESC];
                break;
            case "H": Tab1 = db.query(tableName, null, null, null, null, null, null);
                break;
            default: Tab1 = db.query(tableName, null, "J='"+month+"'", null, null, null, null);
                break;
        }
    }


    private void viewDataBase() {
        id = 3000;
        ll.removeAllViews();
        hsv.removeAllViews();
        sv.removeAllViews();
        int j = 10;
            if (Tab1.moveToFirst()) { // просим курсор перейти к первой записи в таблице
            do {
                tbrow = new TableRow(this);
                tbrow.setBackgroundColor(Color.DKGRAY);
                tbrow.setPadding(0,0,0,2);
                tbrow.setId(id + 0);
                for (int i = 0; i < 10; i++) {
                    tv1 = new TextView(this);
                      tv1.setTextSize(20);tv1.setLeft(2);
                    tv1.setBackgroundColor(Color.WHITE);

                    // tv1.setId(j+0);
                    tv1.setText(Tab1.getString(i + 1));
                    tbrow.addView(tv1);
                    j++;
                }
                tbrow.setOnClickListener(MainActivity.this);
                ll.addView(tbrow);
                id++;
            }
            while (Tab1.moveToNext());
        }
        ll.addView(tRow);
        ll.setId(7000 + 0);
        hsv.addView(ll);
        sv.addView(hsv);
        setContentView(sv);
    }

    @Override
    public void onClick(View v) {
        id = v.getId();
        if (v.getDrawingCacheBackgroundColor()==Color.YELLOW) {v.setBackgroundColor(Color.DKGRAY);v.setDrawingCacheBackgroundColor(Color.LTGRAY);
        }else{
        v.setBackgroundColor(Color.RED);v.setDrawingCacheBackgroundColor(Color.YELLOW);}
        Toast.makeText(getBaseContext(), "ID__" + id, Toast.LENGTH_SHORT).show();
    if (tableName.equals("D")) {
        if (Tab1.moveToPosition(id - 3000)) {
            decPdv = decPdv.add(BigDecimal.valueOf(Double.parseDouble(Tab1.getString(2)) + Double.parseDouble(Tab1.getString(4))).setScale(2, RoundingMode.HALF_UP));
            Toast.makeText(getBaseContext(), "PDV__" + decPdv.toString(), Toast.LENGTH_LONG).show();
        }
    }
        if (flag) {
            if (Tab1.moveToPosition(id - 3000)){decPay=decPay.add(BigDecimal.valueOf(Double.parseDouble(Tab1.getString(1))).setScale(2, RoundingMode.HALF_UP));
        decPayPDV=decPayPDV.add(BigDecimal.valueOf(Double.parseDouble(Tab1.getString(2))).setScale(2, RoundingMode.HALF_UP));
        list.get(1).setText(decPay.toString());
            list.get(2).setText(decPayPDV.toString());
        }
        }
    }

    private void saveHint() {
        cursorSL = null;
        cursorSL = db.query("H", null, "J='" + tableName + "'", null, null, null, null);
        tabHint = tableName;
        tableName = "H";
        if (cursorSL.moveToFirst()) {
            updateBase();
        } else {
            saveBase();
        }
    }

    private void setHint() {
        cursorSL = db.query("H", null, "J='" + tableName + "'", null, null, null, null);
        Log.d("Help", "Coursor_" + cursorSL.toString());
        if (cursorSL.moveToFirst()) {
            for (int i = 1; i < 10; i++) {
                Log.d("Help", "Hint_" + cursorSL.getString(i));
                list.get(i - 1).setHint(cursorSL.getString(i));
            }
        }
    }

    private void coutPDV() {
       if (tableName.equals("D")) {
            bigDecimal = BigDecimal.valueOf(Double.parseDouble(poleS[0]) - Double.parseDouble(poleS[1])).setScale(2,RoundingMode.HALF_UP);
            poleS[4] = String.valueOf(bigDecimal);
            bigDecimal = BigDecimal.valueOf(Double.parseDouble(poleS[2]) - Double.parseDouble(poleS[3])).setScale(2, RoundingMode.HALF_UP);;
            poleS[5] = String.valueOf(bigDecimal);
        }
    }

    private boolean saveBase() {
        setTextEdit();
        cv.put("A", poleS[0]);
        cv.put("B", poleS[1]);
        cv.put("C", poleS[2]);
        cv.put("D", poleS[3]);
        cv.put("E", poleS[4]);
        cv.put("F", poleS[5]);
        cv.put("G", poleS[6]);
        cv.put("H", poleS[7]);
        cv.put("I", poleS[8]);
        //if (tableName.equals("H")){cv.put("J", tableName);}else {
        cv.put("J", poleS[9]);
        //}
        try {
            db.insert(tableName, null, cv);
            Log.d("Help", "OK Data Base is save");
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
        if (!tabHint.equals("X")) {
            tableName = tabHint;
            tabHint = "X";
        }
switch (tableName){
    case "B":countCasa();
        break;
    case "C":countBank();
        break;
    default:
        break;
}
        return true;
    }

    private void setTextEdit() {
        for (int i = 0; i < 10; i++) {
            poleS[i] = "0";
            poleS[i] = list.get(i).getText().toString();
            if (i < 8) {
                list.get(i).setText("");

            }

        }
        list.get(0).setFocusableInTouchMode(true);
        list.get(0).requestFocus();
        coutPDV();
    }

    private void updateBase() {
        setTextEdit();
        cv.put("A", poleS[0]);
        cv.put("B", poleS[1]);
        cv.put("C", poleS[2]);
        cv.put("D", poleS[3]);
        cv.put("E", poleS[4]);
        cv.put("F", poleS[5]);
        cv.put("G", poleS[6]);
        cv.put("H", poleS[7]);
        cv.put("I", poleS[8]);
        cv.put("J", poleS[9]);
       baseId = Integer.parseInt(strId);
        int updCount = db.update(tableName, cv, "id = " + baseId, null);
        Log.d("Help", "OK" + updCount);
        if (!tabHint.equals("X")) {
            tableName = tabHint;
            tabHint = "X";
        }
    }


    private void saveFile() {
        // проверяем доступность SD
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            Log.d("Help", "SD card isnt write!ERROR!");
            return;
        }
        // получаем путь к SD
        File sdPath = Environment.getExternalStorageDirectory();
        // добавляем свой каталог к пути
        sdPath = new File(sdPath.getAbsolutePath() + "/" + DIR_SD);
        // созаем каталог
        sdPath.mkdirs();
        // формируем объект File, который содержит путь к файлу
        File sdFile = new File(sdPath, FILENAME_SD);
        try {
            // открываем поток для записи
            BufferedWriter bw = new BufferedWriter(new FileWriter(sdFile));
            if (Tab1 == null) {
                Log.d("Help", "Cursor is null!!!!!!!!!!!!!!!");
            } else {
                if (Tab1.moveToFirst()) {
                    do
                    {
                        string = "";
                        for (int i = 1; i < 11; i++) {
                            string = string + Tab1.getString(i) + ";";
                        }
                        bw.write(string);
                        bw.newLine();
                    }
                    while (Tab1.moveToNext());
                }
            }
            // закрываем поток
            bw.close();
            // Log.d("Help", "SD card isnt write!ERROR!");
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d("Help", "File write to SD.");
        Toast.makeText(getBaseContext(), "файл записаний", Toast.LENGTH_SHORT).show();
    }

    private void saveInFile() {
        // получаем путь к SD
        File sdPath = Environment.getExternalStorageDirectory();
        // добавляем свой каталог к пути
        sdPath = new File(sdPath.getAbsolutePath() + "/" + DIR_SD);
        // создаем каталог
        sdPath.mkdirs();
        // формируем объект File, который содержит путь к файлу
        string = "NanoBug" + tableName + ".txt";
        File sdFile = new File(sdPath, string);
        try {
            PrintWriter pw = new PrintWriter(sdFile);
            if (Tab1.moveToFirst()) {
                do {
                    string = "";
                    for (int i = 1; i < 11; i++) {
                        string = string + Tab1.getString(i) + ";";
                    }
                    pw.println(string);
                }
                while (Tab1.moveToNext());
            } pw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(getBaseContext(), "неможливо записати", Toast.LENGTH_SHORT).show();
        }

        Toast.makeText(getBaseContext(), "файл записаний", Toast.LENGTH_SHORT).show();
    }

    private void tableItemStart() {
        tRow = new TableRow(this);
        tRow.setId(5000 + 0);
        for (int i = 1000; i < 1010; i++) {
            et = new EditText(this);
            et.setId(i + 0);
            if (i == 1009) {
                et.setHint("month.year");
            }
            list.add(et);
            tRow.addView(et);
        }
    }

    private boolean countCasa() {
    bigDecimal = BigDecimal.ZERO;
            aDouble = 0.00;
            cursorSL = db.query("H", null, "J= 'CB'", null, null, null, null);
            if (cursorSL.moveToFirst()) {
                aDouble = Double.valueOf(cursorSL.getString(1));
            }
            aDouble = aDouble + Double.parseDouble(poleS[0]) + Double.parseDouble(poleS[1]) + Double.parseDouble(poleS[2]) - Double.parseDouble(poleS[3])
                    - Double.parseDouble(poleS[4]) - Double.parseDouble(poleS[5]) - Double.parseDouble(poleS[6]) - Double.parseDouble(poleS[7]);
            bigDecimal = BigDecimal.valueOf(aDouble);
            bigDecimal = bigDecimal.setScale(2, RoundingMode.HALF_UP);

            Toast.makeText(getBaseContext(), "Залишок__" + bigDecimal.toString(), Toast.LENGTH_LONG).show();
            cv.put("A", bigDecimal.toString());
            cv.put("I", "Casa");
            cv.put("J", "CB");

            try {
                db.update("H", cv, "J='CB'", null);
                Log.d("Help", "OK Data Base is save");
            } catch (Exception e) {
                e.printStackTrace();
                return true;
            }
            return false;
    }


    private void delItem(){
        id=id-3000;
        // SQLiteDatabase         db2 = dbHelper.getWritableDatabase();
        //int count = db.delete(tableName,"id=?",new String[]{id.toString()});
        Tab1.moveToPosition(id);
        baseId=Tab1.getInt(0);
        int count= db.delete(tableName, "id = "+baseId, null);
        Log.d("Help", "Delete item" + count + "_" + id + "_" + tableName);

    }

    private void gotoUpdate(){
        id=id-3000;
        Tab1.moveToPosition(id);
        for (int i=0; i<10;i++) {
            list.get(i).setText(Tab1.getString(i+1));
                   } strId=Tab1.getString(0);
        Log.d("Help","ID___"+strId);
    }

    private boolean countBank(){
        bigDecimal = BigDecimal.ZERO;
        aDouble = 0.00;
        cursorSL = db.query("H", null, "J= 'CC'", null, null, null, null);
        if (cursorSL.moveToFirst()) {
            aDouble = Double.valueOf(cursorSL.getString(1));
        }
        aDouble = aDouble + Double.parseDouble(poleS[0]) + Double.parseDouble(poleS[1])- Double.parseDouble(poleS[3]) - Double.parseDouble(poleS[5]) - Double.parseDouble(poleS[7]);
        bigDecimal = (BigDecimal.valueOf(aDouble)).setScale(2, RoundingMode.HALF_UP);
        Toast.makeText(getBaseContext(), "Залишок__" + bigDecimal.toString(), Toast.LENGTH_LONG).show();
        cv.put("A", bigDecimal.toString());
        cv.put("J", "CC");
        try {
            db.update("H", cv, "J='CC'", null);
            Log.d("Help", "OK Data Base is save");
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
     return false;
    }

    public void run() {
        // Create a shiny new (but blank) PDF document in memory
        // We want it to optionally be printable, so add PrintAttributes
        // and use a PrintedPdfDocument. Simpler: new PdfDocument().
        PrintAttributes printAttrs = new PrintAttributes.Builder().
                setColorMode(PrintAttributes.COLOR_MODE_COLOR).
                setMediaSize(PrintAttributes.MediaSize.NA_LETTER).
                setResolution(new PrintAttributes.Resolution("zooey", PRINT_SERVICE, 1920, 1080)).
                setMinMargins(PrintAttributes.Margins.NO_MARGINS).
                build();
        PdfDocument document = new PrintedPdfDocument(this, printAttrs);
        // crate a page description
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(1920, 1080, 1).create();
        // create a new page from the PageInfo
        PdfDocument.Page page = document.startPage(pageInfo);
        // repaint the user's text into the page
        View content = findViewById(7000+0);
        content.draw(page.getCanvas());


        // do final processing of the page
        document.finishPage(page);
        // Here you could add more pages in a longer doc app, but you'd have
        // to handle page-breaking yourself in e.g., write your own word processor...
        // Now write the PDF document to a file; it actually needs to be a file
        // since the Share mechanism can't accept a byte[]. though it can
        // accept a String/CharSequence. Meh.
        try {
            File sdPath = Environment.getExternalStorageDirectory();
            // добавляем свой каталог к пути
            sdPath = new File(sdPath.getAbsolutePath() + "/" + "PDFDir");
            // создаем каталог
            sdPath.mkdirs();
            // формируем объект File, который содержит путь к файлу
            File sdFile = new File(sdPath, "MyPdf.pdf");


            FileOutputStream fos = new FileOutputStream(sdFile);
            document.writeTo(fos);
            document.close();
            fos.close();
        } catch (IOException e) {
            throw new RuntimeException("Error generating file", e);
        }
    }

    private void openFile(String fileName) {
       builder= new StringBuilder();
       builder2= new StringBuilder() ;

        try {
            InputStream inputStream = openFileInput(fileName);

            if (inputStream != null) {
                InputStreamReader isr = new InputStreamReader(inputStream);
                BufferedReader reader = new BufferedReader(isr);
                String line;
                while ((line = reader.readLine()) != null) {
                    if (line.equals("<--!-->")){builder2.append(builder);builder.delete(0,builder.length());}
                    builder.append(line + "\n");
                }

                inputStream.close();
               // mEditText.setText(builder.toString());
            }
        } catch (Throwable t) {
            Toast.makeText(getApplicationContext(),
                    "Exception: " + t.toString(), Toast.LENGTH_LONG).show();
        }
    }

}