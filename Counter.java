package com.example.adminn.appproj090216;

import android.util.Log;

/**
 * Created by adminn on 16.03.2016.
 */
public class Counter {
    public String countMonth(String month){
        String string= month.substring(0,2);
        Integer inr=Integer.parseInt(string)-1;
        Log.d("Count",inr.toString());
        String monthS = inr.toString();
        if (inr<11){monthS="0"+inr.toString();}
        Log.d("Count",monthS);
        return month.replaceFirst(string, monthS);
    }
}
