package com.example.adminn.appproj090216;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DataBase extends SQLiteOpenHelper {
    public DataBase(Context context) {
        super(context, "_MyDataBase", null, 9);
        //Конструктор для создания базы данных}
    }

    public void onCreate(SQLiteDatabase db)
    {
        //Создание таблиц


        db.execSQL
                ("create table A ("//таблиця постачальник
                        + "id integer primary key autoincrement,"//создаем поле таблицы id оно обезательное
                        + "A text,"
                        + "B text,"
                        + "C text,"
                        + "D text,"
                        + "E text,"
                        + "F text,"
                        + "G text,"
                        + "H text,"
                        + "I text,"
                        + "J text"
                        +");");

        //таблиця каса
        db.execSQL
                ("create table B ("
                        + "id integer primary key autoincrement,"//создаем поле таблицы id оно обезательное
                        + "A text,"
                        + "B text,"
                        + "C text,"
                        + "D text,"
                        + "E text,"
                        + "F text,"
                        + "G text,"
                        + "H text,"
                        + "I text,"
                        + "J text"
                        +");");
        //таблиця банк
        db.execSQL
                ("create table C ("
                        + "id integer primary key autoincrement,"//создаем поле таблицы id оно обезательное
                        + "A text,"
                        + "B text,"
                        + "C text,"
                        + "D text,"
                         + "E text,"
                        + "F text,"
                        + "G text,"
                        + "H text,"
                        + "I text,"
                        + "J text"
                        +");");
        //таблиця щоденного ПДВ
        db.execSQL
                ("create table D ("
                        + "id integer primary key autoincrement,"//создаем поле таблицы id оно обезательное
                        + "A text,"
                        + "B text,"
                        + "C text,"
                        + "D text,"
                        + "E text,"
                        + "F text,"
                        + "G text,"
                        + "H text,"
                        + "I text,"
                        + "J text"
                        +");");
        // таблиця зарплата
        db.execSQL
                ("create table E ("
                        + "id integer primary key autoincrement,"//создаем поле таблицы id оно обезательное
                        + "A text,"
                        + "B text,"
                        + "C text,"
                        + "D text,"
                        + "E text,"
                        + "F text,"
                        + "G text,"
                        + "H text,"
                        + "I text,"
                        + "J text"
                        +");");
        //таблиця залишків
        db.execSQL
                ("create table F ("
                        + "id integer primary key autoincrement,"//создаем поле таблицы id оно обезательное
                        + "A text,"
                        + "B text,"
                        + "C text,"
                        + "D text,"
                        + "E text,"
                        + "F text,"
                        + "G text,"
                        + "H text,"
                        + "I text,"
                        + "J text"
                        +");");
        //таблиця результат місяця
        db.execSQL
                ("create table G ("
                        + "id integer primary key autoincrement,"//создаем поле таблицы id оно обезательное
                        + "A text,"
                        + "B text,"
                        + "C text,"
                        + "D text,"
                        + "E text,"
                        + "F text,"
                        + "G text,"
                        + "H text,"
                        + "I text,"
                        + "J text"
                        +");");
//службова таблиця
        //таблиця вибору постач, підр, операцій банку та каси
        db.execSQL
                ("create table H ("
                        + "id integer primary key autoincrement,"//создаем поле таблицы id оно обезательное
                        + "A text,"
                        + "B text,"
                        + "C text,"
                        + "D text,"
                        + "E text,"
                        + "F text,"
                        + "G text,"
                        + "H text,"
                        + "I text,"
                        + "J text"

                        +");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS A");
        db.execSQL("DROP TABLE IF EXISTS B");
        db.execSQL("DROP TABLE IF EXISTS C");
        db.execSQL("DROP TABLE IF EXISTS D");
        db.execSQL("DROP TABLE IF EXISTS E");
        db.execSQL("DROP TABLE IF EXISTS F");
        db.execSQL("DROP TABLE IF EXISTS G");
        db.execSQL("DROP TABLE IF EXISTS H");
        onCreate(db);
    }}
