package com.example.adminn.appproj090216;


public class FromDB {
private     int id;
private     String A;
private     String B;
private     String C;
private     String D;
private     String E;
private     String F;
private     String G;
private     String H;
private     String I;
private     String J;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getA() {
        return A;
    }

    public void setA(String a) {
        A = a;
    }

    public String getB() {
        return B;
    }

    public void setB(String b) {
        B = b;
    }

    public void setC(String c) {
        C = c;
    }

    public String getD() {
        return D;
    }

    public void setD(String d) {
        D = d;
    }

    public String getE() {
        return E;
    }

    public void setE(String e) {
        E = e;
    }

    public String getF() {
        return F;
    }

    public void setF(String f) {
        F = f;
    }

    public String getG() {
        return G;
    }

    public void setG(String g) {
        G = g;
    }

    public String getH() {
        return H;
    }

    public void setH(String h) {
        H = h;
    }

    public String getI() {
        return I;
    }

    public void setI(String i) {
        I = i;
    }

    public String getJ() {
        return J;
    }

    public void setJ(String j) {
        J = j;
    }

    public String getC() {
        return C;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        FromDB fromDB = (FromDB) o;

        if (id != fromDB.id)
            return false;
        if (A != null ? !A.equals(fromDB.A) : fromDB.A != null)
            return false;
        if (B != null ? !B.equals(fromDB.B) : fromDB.B != null)
            return false;
        if (C != null ? !C.equals(fromDB.C) : fromDB.C != null)
            return false;
        if (D != null ? !D.equals(fromDB.D) : fromDB.D != null)
            return false;
        if (E != null ? !E.equals(fromDB.E) : fromDB.E != null)
            return false;
        if (F != null ? !F.equals(fromDB.F) : fromDB.F != null)
            return false;
        if (G != null ? !G.equals(fromDB.G) : fromDB.G != null)
            return false;
        if (H != null ? !H.equals(fromDB.H) : fromDB.H != null)
            return false;
        if (I != null ? !I.equals(fromDB.I) : fromDB.I != null)
            return false;
        return !(J != null ? !J.equals(fromDB.J) : fromDB.J != null);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (A != null ? A.hashCode() : 0);
        result = 31 * result + (B != null ? B.hashCode() : 0);
        result = 31 * result + (C != null ? C.hashCode() : 0);
        result = 31 * result + (D != null ? D.hashCode() : 0);
        result = 31 * result + (E != null ? E.hashCode() : 0);
        result = 31 * result + (F != null ? F.hashCode() : 0);
        result = 31 * result + (G != null ? G.hashCode() : 0);
        result = 31 * result + (H != null ? H.hashCode() : 0);
        result = 31 * result + (I != null ? I.hashCode() : 0);
        result = 31 * result + (J != null ? J.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "FromDB{" +
                "id=" + id +
                ", A='" + A + '\'' +
                ", B='" + B + '\'' +
                ", C='" + C + '\'' +
                ", D='" + D + '\'' +
                ", E='" + E + '\'' +
                ", F='" + F + '\'' +
                ", G='" + G + '\'' +
                ", H='" + H + '\'' +
                ", I='" + I + '\'' +
                ", J='" + J + '\'' +
                '}';
    }


}
