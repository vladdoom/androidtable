package com.example.adminn.appproj090216;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by adminn on 22.02.2016.
 */
public class MakeHTML {
    private  String firmName;
    private Cursor Tab1;
    private Cursor cursorSL;
    boolean flag2;
    boolean flag;
    PrintWriter pw ;
    StringBuilder builder;
    StringBuilder builder2;

    public MakeHTML(String firmName, Cursor Tab1, Cursor cursorSL,StringBuilder builder,StringBuilder builder2) throws FileNotFoundException {
        this.firmName=firmName;
        this.Tab1=Tab1;
        this.cursorSL=cursorSL;
        this.builder=builder;
        this.builder2=builder2;

        File sdPath = Environment.getExternalStorageDirectory();
        // добавляем свой каталог к пути
        sdPath = new File(sdPath.getAbsolutePath() + "/" + "MyHTML");
        // создаем каталог
        sdPath.mkdirs();
        // формируем объект File, который содержит путь к файлу
        File sdFile = new File(sdPath, firmName+".html");

           pw = new PrintWriter(sdFile);
          makeHtmlPostach(pw);
    }




    private void makeHtmlPostach(PrintWriter pw){
        //Tab1=db.query("A",null,"F='v'",null,null,null,null);
        //cursorSL=db.query("C",null,null,null,null,null,null);
pw.println(builder);
        pw.println("<caption>Взаєморозрахунки з постачальником за <i>лютий</i> 2016р.</caption> ");
        switch (firmName){
            case "o": pw.println("<caption>СП ''Оптіма-Фарм.ЛТД''</caption>");
                pw.println("<caption >На початок місяця Кт рах.63112: <i></i>грн.</caption>" );
                break;
            case "v": pw.println("<caption>ТзОВ ''Вента.ЛТД''</caption>");
                pw.println("<caption >На початок місяця Кт рах.63112: <i></i>грн.</caption>" );
                break;
            case "k": pw.println("<caption>ПП ''Конекс''</caption>");
                pw.println("<caption >На початок місяця Кт рах.63112: <i></i>грн.</caption>" );
                break;
            case "f": pw.println("<caption>ТОВ ''ФРАМ КО''</caption>");
                pw.println("<caption >На початок місяця Кт рах.63112: <i></i>грн.</caption>" );
                break;
        }


        if (Tab1.moveToFirst()&&cursorSL.moveToFirst()){int i;
            do {if (Tab1.getString(6).equals("v")){i=10;
               pw.println( "<tr> <td>"+Tab1.getString(i)+"</td>");  countRezult(i);i=9;
                pw.println("<td>"+Tab1.getString(i)+"</td>");countRezult(i); i=1;
                pw.println("<td>"+Tab1.getString(i)+"</td>");countRezult(i);i++;
                pw.println("<td>"+Tab1.getString(i)+"</td>");countRezult(i);  i++;
                pw.println("<td>"+countSumaBezPDV(Tab1.getString(2), Tab1.getString(1), i).toString()+"</td>"); i++;
                pw.println("<td>" + Tab1.getString(i) + "</td>");countRezult(i); i++;
                pw.println("<td>"+Tab1.getString(i)+"</td>");countRezult(i); i++;
                pw.println("<td>"+countSumaBezPDV(Tab1.getString(3), Tab1.getString(4), i).toString()+"</td>");    countRezult(i);
Log.d("Write","Write html.");
                writePay(pw);
            }
            }while (Tab1.moveToNext());
             pw.println("<tr>");
            for (int j=0;j<6;j++){
                pw.println("<td>"+bigDcimalMas[j]+"</td>");}
            pw.println("<td>X </td>");
            pw.println("<td>X </td>");
            pw.println("<td>"+bigDcimalMas[6]+"</td>");

            pw.println("</tr>");
        }
    pw.println(builder2);
        pw.close();

    }


    private void writePay(PrintWriter pw){
        int h=3;
        if (flag){
            if (cursorSL.moveToNext()){
                if (!cursorSL.getString(3).equals("0")){h++;}
                if (!cursorSL.getString(5).equals("0")){h++;}
                if (!cursorSL.getString(7).equals("0")){h++;}
            }else{
                flag=false;
            }
        }else {
            pw.println("<td> </td>");
            pw.println("<td> </td>");
            pw.println("<td> </td>");
        }
    }

    BigDecimal[] bigDcimalMas = new BigDecimal[10];
    private BigDecimal countSumaBezPDV(String s1, String s2, int i){
        return bigDcimalMas[i]= BigDecimal.valueOf(Double.parseDouble(s1)-Double.parseDouble(s2)).setScale(2, RoundingMode.HALF_UP);
    }

    private void countRezult(int i){
        bigDcimalMas[i] = bigDcimalMas[i].add(BigDecimal.valueOf(Double.parseDouble(Tab1.getString(i+1)))).setScale(2, RoundingMode.HALF_UP);
    }
}
